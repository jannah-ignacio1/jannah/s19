/*Activity:
1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
12. Create/instantiate a new object from the class Dog and console log the object.
13. Create a git repository named S19.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.*/


const getCube = 5 ** 3;
	console.log(`The cube of 5 is ${getCube}`)

const address = [258, "Washington Ave", "NW", "California", 90011]
const [streetNum, streetName, district, country, zipCode] = address;
console.log(`I live at ${streetNum} ${streetName} ${district}, ${country} ${zipCode}`)


const animals = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}
const {name, species, weight, length} = animals
console.log(`${name} is a ${species}. He weighed at ${weight} with a measurement of ${length}.`)

const number = [1, 2, 3, 4, 5]
number.forEach( (numbers) => {
		console.log(`${numbers}`)
	}
	)


class Dog {
	constructor(name, age, breed) {
		this.name = "Frankie";
		this.age = 5;
		this.brand = "Miniature Dachshund"
	}
}
const myDog = new Dog
console.log(myDog)

myDog.color = "brown";
console.log(myDog)








